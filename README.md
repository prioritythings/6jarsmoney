# Technical

## API: 

Language: NODEJS
Framework : https://sailsjs.com ( javascript )

## BO:

Language : NodeJS
Framework: https://expressjs.com/

## DB

Database: MongoDB - https://mlab.com/plans/
Database: MySQL - server - free with heroku

## Mailer

- Free Email: https://ethereal.email/
- Trial: MailJet, SendGrid
- Gmail SMTP

## Images

- Free: upload dir
- Social: Facebook, Instagram
- Cloud: S3

## FO

Languages: Typesript, HTML,SASS
Framework: https://angular.io/
CSSFramework : https://material.angular.io/